GOPATH ?= $(HOME)/go
PATH := $(GOPATH)/bin:$(PATH)
DESTDIR ?= /

.PHONY: all
all: bins

.PHONY: bins
bins:
	mkdir -p bin
	go build -o bin/installer cmd/installer/*.go
	go build -o bin/installer-gui cmd/installer-gui/*.go

.PHONY: clean
clean:
	rm -rf bin/

.PHONY: deps
deps:
	go get ./...

.PHONY: fmt
fmt:
	find cmd/ internal/ -name '*.go' | xargs gofmt -w -s

.PHONY: vendor
vendor:
	GO111MODULE=on go mod vendor

.PHONY: test
test:
	@echo "TODO: we should have some tests"

.PHONY: install
install:
	mkdir -p $(DESTDIR)/$(bindir)
	install -m 0755 bin/installer-gui $(DESTDIR)/$(bindir)
	install -m 0755 bin/installer $(DESTDIR)/$(bindir)

.PHONY: check
check: golint all test
	DESTDIR=/tmp make install
	go install `go list -f  "{{.ImportPath}}" "{{.TestGoFiles}}" ./...`

.PHONY: golint
golint:
	golangci-lint --verbose run --enable-all -Dgochecknoglobals -Dgochecknoinits -Dlll

