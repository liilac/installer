// Copyright 2018 Assured Information Security, Inc. All Rights Reserved.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package installer

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io"
	"io/ioutil"
	"log"
	"log/syslog"
	"net/http"
	"net/url"
	"os"
	"os/exec"
	"path"
	"strings"
	"syscall"
	"time"
)

// RemoteInstallAuth Authentication information for management server
type RemoteInstallAuth struct {
	Username    string
	Password    string
	Server      string
	Certificate string
}

// InstallInfo State information for both local and remote installations
type InstallInfo struct {
	BlockDevice string
	DeviceUUID  string
	ImagePath   string
	Upgrade     bool
	RemoteAuth  RemoteInstallAuth
	FdeInfo     FdeFunctions
}

// ProductUUID Device's UUID from sysfs
var ProductUUID string

// LoggedCommand Execute a command log it and arguments to the log
func LoggedCommand(name string, arg ...string) *exec.Cmd {
	log.Printf("executing: %s %s\n", name, strings.Join(arg, " "))
	return exec.Command(name, arg...)
}

func checkSelectedDisk(d string) bool {
	if d == "" {
		return false
	}

	rootMount, err := LoggedCommand("mount").CombinedOutput()
	log.Print(string(rootMount))
	var rootDevice string
	disk := strings.Split(d, "/")[len(strings.Split(d, "/"))-1]

	if err != nil {
		return false
	}

	rootMountOutput := bytes.Split(rootMount, []byte("\n"))
	for _, mount := range rootMountOutput {
		if bytes.Contains(mount, []byte("on / type")) {
			rootDevice = string(mount)
		}
	}

	blkdevs, err := ioutil.ReadDir("/sys/block")
	if err != nil {
		return false
	}

	for _, blkdev := range blkdevs {
		if strings.Contains(blkdev.Name(), "dm") ||
			strings.Contains(blkdev.Name(), "loop") ||
			strings.Contains(blkdev.Name(), "ram") ||
			strings.Contains(blkdev.Name(), "sr") ||
			strings.Contains(blkdev.Name(), rootDevice) {
			log.Print("Ignoring device " + blkdev.Name())
		} else if strings.Contains(blkdev.Name(), disk) {
			return true
		}
	}

	return false
}

func blockDevicePathWithPhysicalVolume(physicalVolumePath string) string {
	blockDevice := strings.TrimSuffix(physicalVolumePath, "2")
	blockDevice = strings.TrimSuffix(blockDevice, "p")

	return blockDevice
}

//func blockDevicePathWithESPPartition(espPartition string) string {
//	blockDevice := strings.TrimSuffix(espPartition, "1")
//	blockDevice = strings.TrimSuffix(blockDevice, "p")
//
//	return blockDevice
//}

func getPhysicalVolume() string {
	pvDisplay, err := LoggedCommand("pvdisplay", "-c").Output()
	if err != nil {
		return ""
	}

	pvDisplayOutput := bytes.Split(pvDisplay, []byte(":"))

	return strings.TrimSpace(string(pvDisplayOutput[0]))
}

func espPartition(blockdevice string) string {
	var espPartition string

	if strings.Contains(blockdevice, "nvme") || strings.Contains(blockdevice, "nbd") {
		espPartition = blockdevice + "p1"
	} else {
		espPartition = blockdevice + "1"
	}

	return espPartition
}

func physicalVolumePartition(blockdevice string) string {
	var physicalVolumePartition string

	if strings.Contains(blockdevice, "nvme") || strings.Contains(blockdevice, "nbd") {
		physicalVolumePartition = blockdevice + "p2"
	} else {
		physicalVolumePartition = blockdevice + "2"
	}

	return physicalVolumePartition
}

func mountPartition(partition string, mountPoint string) bool {
	mountProc, err := LoggedCommand("mount", partition, mountPoint).CombinedOutput()
	log.Print(string(mountProc))
	if err != nil {
		log.Print("Failed to mount: " + partition + " on mount point: " + mountPoint)
		return false
	}

	return true
}

func pollDevnode(devnode string, timeout int) bool {
	for fi, err := os.Stat(devnode); (os.IsNotExist(err) || (fi.Mode()&os.ModeDevice) == 0) && timeout > 0; timeout-- {
		<-time.After(1 * time.Second)
	}
	return timeout > 0
}

func unmountPartition(mountPoint string) bool {
	mountProc, err := LoggedCommand("umount", mountPoint).CombinedOutput()
	log.Print(string(mountProc))
	if err != nil {
		log.Print("Failed to unmount: " + mountPoint)
		return false
	}

	return true
}

func copyFile(src string, dst string) bool {
	source, err := os.Open(src)
	if err != nil {
		log.Print(err.Error())
		return false
	}
	defer source.Close()

	destination, err := os.Create(dst)
	if err != nil {
		log.Print(err.Error())
		return false
	}
	defer destination.Close()

	_, err = io.Copy(destination, source)
	if err != nil {
		log.Print(err.Error())
		return false
	}

	err = destination.Sync()
	if err != nil {
		log.Print(err.Error())
		return false
	}

	return true
}

func scanLogicalVolumes() bool {
	activatePhysicalVolume, err := LoggedCommand("pvscan").CombinedOutput()
	log.Print(string(activatePhysicalVolume))
	if err != nil {
		log.Print("Failed to activate physical volume.")
		return false
	}

	scanVolumeGroup, err := LoggedCommand("vgscan").CombinedOutput()
	log.Print(string(scanVolumeGroup))
	if err != nil {
		log.Print("Failed to scan volume group dom0")
		return false
	}

	scanLogicalVolumes, err := LoggedCommand("lvscan", "--all").CombinedOutput()
	log.Print(string(scanLogicalVolumes))
	if err != nil {
		log.Print("Failed to scan logical volumes")
		return false
	}

	return true
}

func activateLogicalVolumes(volumeGroup string) bool {
	activateLogicalVolumesCmd, err := LoggedCommand("lvchange", "-ay", volumeGroup).CombinedOutput()
	log.Print(string(activateLogicalVolumesCmd))
	if err != nil {
		log.Print("Failed to activate logical volumes")
		return false
	}

	return true
}

func createGpt(blkdev string) bool {
	// Make the GPT
	gpt, err := LoggedCommand("parted", "--script", blkdev, "mklabel", "GPT").CombinedOutput()
	log.Print(string(gpt))
	if err != nil {
		log.Print("Failed to create GPT on " + blkdev)
		return false
	}

	return true
}

func createEsp(blkdev string) bool {
	espCreate, err := LoggedCommand("parted", "--script", blkdev, "mkpart", "ESP", "fat32", "1MiB", "512MiB").CombinedOutput()
	log.Print(string(espCreate))
	if err != nil {
		log.Print("Failed to create ESP partition.")
		return false
	}

	espSet, err := LoggedCommand("parted", "--script", blkdev, "set", "1", "esp", "on").CombinedOutput()
	log.Print(string(espSet))
	if err != nil {
		log.Print("Failed to create ESP partition.")
		return false
	}
	if !pollDevnode(espPartition(blkdev), 10) {
		log.Print("ESP partition is not ready after timeout.")
		return false
	}

	mkfs, err := LoggedCommand("mkfs.fat", espPartition(blkdev)).CombinedOutput()
	log.Print(string(mkfs))
	if err != nil {
		log.Print("Failed to create FAT file system.")
		return false
	}

	return true
}

func createLogicalVolumes(blkdev string) bool {
	createPartition, err := LoggedCommand("parted", "--script", blkdev, "mkpart", "dom0", "ext4", "512MiB", "100%").CombinedOutput()
	log.Print(string(createPartition))
	if err != nil {
		log.Print("Failed to create base partition for LVM.")
		return false
	}
	if !pollDevnode(physicalVolumePartition(blkdev), 10) {
		log.Print("Base partition is not ready after timeout.")
		return false
	}

	createPhysicalVolume, err := LoggedCommand("pvcreate", "-ff", "-y", physicalVolumePartition(blkdev)).CombinedOutput()
	log.Print(string(createPhysicalVolume))
	if err != nil {
		log.Print("Failed to create physical volume.")
		return false
	}

	activatePhysicalVolume, err := LoggedCommand("pvscan").CombinedOutput()
	log.Print(string(activatePhysicalVolume))
	if err != nil {
		log.Print("Failed to activate physical volume.")
		return false
	}

	createVolumeGroup, err := LoggedCommand("vgcreate", "dom0", physicalVolumePartition(blkdev)).CombinedOutput()
	log.Print(string(createVolumeGroup))
	if err != nil {
		log.Print("Failed to create volume group.")
		return false
	}

	activateVolumeGroup, err := LoggedCommand("vgscan").CombinedOutput()
	log.Print(string(activateVolumeGroup))
	if err != nil {
		log.Print("Failed to activate volume group.")
		return false
	}

	createLogicalVolume, err := LoggedCommand("lvcreate", "-y", "--name", "storage", "-l95%VG", "dom0").CombinedOutput()
	log.Print(string(createLogicalVolume))
	if err != nil {
		log.Print("Failed to create logical volume.")
		return false
	}

	return true
}

func createPartitionTables(blkdev string) bool {
	removeVolumeGroup, _ := LoggedCommand("vgremove", "-ff", "-y", "dom0").CombinedOutput()
	log.Print(string(removeVolumeGroup))
	success := createGpt(blkdev)
	if !success {
		return false
	}

	success = createEsp(blkdev)
	if !success {
		return false
	}

	success = createLogicalVolumes(blkdev)
	return success
}

func closeLuksVolume(luksVolumeName string) bool {
	closeLuks, err := LoggedCommand("cryptsetup", "luksClose", luksVolumeName).CombinedOutput()
	log.Print(string(closeLuks))
	if err != nil {
		log.Print("Failed to close encrypted storage volume.")
		return false
	}

	return true
}

func createEncryptedStoragePartition(info InstallInfo) bool {
	if !info.FdeInfo.EncryptDevice("/dev/mapper/dom0-storage") {
		return false
	}

	if !info.FdeInfo.OpenDevice("/dev/mapper/dom0-storage", "storage") {
		return false
	}

	mkfs, err := LoggedCommand("mkfs.ext4", "/dev/mapper/storage").CombinedOutput()
	log.Print(string(mkfs))
	if err != nil {
		log.Print("Failed to create ext4 file system on storage.")
		return false
	}

	return true
}

func mountEspPartition(espPartition string) bool {
	createEspMountPoint, err := LoggedCommand("mkdir", "-p", "/tmp/target-efi").CombinedOutput()
	log.Print(string(createEspMountPoint))
	if err != nil {
		log.Print("Failed to create mount point for EFI System Partition")
		return false
	}

	if !mountPartition(espPartition, "/tmp/target-efi") {
		log.Print("Failed to mount EFI System Partition.")
		return false
	}

	return true
}

func unmountEspPartition() bool {
	if !unmountPartition("/tmp/target-efi") {
		log.Print("Failed to umount ESP partition.")
		return false
	}
	if os.Remove("/tmp/target-efi") != nil {
		log.Print("Failed to remove ESP partition mount-point.")
		return false
	}
	return true
}

func mountStoragePartition() bool {
	mountStorageSuccess := mountPartition("/dev/mapper/storage", "/storage")
	if !mountStorageSuccess {
		log.Print("Failed to mount storage partition")
		return false
	}
	return true
}

func unmountStoragePartition() {
	if !unmountPartition("/storage") {
		log.Print("Failed to umount storage partition.")
	}
}

func setupEsp(espPartition string, shim string, xen string, config string, kernel string, initramfs string) bool {
	if !mountEspPartition(espPartition) {
		log.Print("Failed to mount ESP parition.")
		return false
	}
	defer unmountEspPartition()

	if os.MkdirAll("/tmp/target-efi/EFI/BOOT", 0777) != nil {
		log.Print("Failed to create EFI System Partition layout.")
		return false
	}

	if !copyFile(shim, "/tmp/target-efi/EFI/BOOT/BOOTX64.EFI") {
		log.Print("Failed to place shim.")
		return false
	}

	if !copyFile(config, "/tmp/target-efi/EFI/BOOT/BOOTX64.CFG") {
		log.Print("Failed to place EFI configuration file.")
		return false
	}

	if !copyFile(xen, "/tmp/target-efi/EFI/BOOT/xen.efi") {
		log.Print("Failed to place Xen.")
		return false
	}

	if !copyFile(kernel, "/tmp/target-efi/EFI/BOOT/bzImage") {
		log.Print("Failed to place kernel.")
		return false
	}

	if !copyFile(initramfs, "/tmp/target-efi/EFI/BOOT/initramfs.gz") {
		log.Print("Failed to place initramfs.")
		return false
	}

	return true
}

func removeOldEFIBootEntries(label string) bool {
	efiBootEntries, err := LoggedCommand("efibootmgr").CombinedOutput()
	if err != nil {
		log.Print("Failed to run efibootmgr.")
		return false
	}
	for _, l := range strings.Split(strings.TrimSuffix(string(efiBootEntries), "\n"), "\n") {
		var id int
		var lbl string
		n, _ := fmt.Sscanf(l, "Boot%X* %s", &id, &lbl)
		if n == 2 && lbl == label {
			_, err := LoggedCommand("efibootmgr", "-b", fmt.Sprintf("%04X", id), "-B").CombinedOutput()
			if err != nil {
				log.Printf("Failed to remove EFI boot entry: %s.", l)
			}
		}

	}
	return true
}

func setupEFIBootEntry(espBlockDev string, peBinary string, label string) bool {
	if !removeOldEFIBootEntries(label) {
		return false
	}

	registerEfiBootEntry, err := LoggedCommand("efibootmgr",
		"--create", "--disk", espBlockDev, "--part", "1",
		"--loader", peBinary, "--label", label,
		"--verbose").CombinedOutput()
	log.Print(string(registerEfiBootEntry))
	if err != nil {
		log.Print("Failed to setup default EFI boot entry.")
		return false
	}

	return true
}

func prepareDiskInstallation(info InstallInfo) bool {
	scanLogicalVolumes()

	blkdevFD, err := os.Open(info.BlockDevice)
	if err != nil {
		return false
	}
	/* Defer will keep the fd open until this function returns. */
	defer blkdevFD.Close()

	if syscall.Flock(int(blkdevFD.Fd()), syscall.LOCK_EX) != nil {
		return false
	}

	if !createPartitionTables(info.BlockDevice) {
		return false
	}

	if !createEncryptedStoragePartition(info) {
		return false
	}

	return true
}

func modifyEfiConfiguration(info InstallInfo, efiConfig string) bool {
	config, err := ioutil.ReadFile(efiConfig)
	if err != nil {
		log.Print(err)
		return false
	}

	lines := strings.Split(string(config), "\n")
	for i, line := range lines {
		if strings.Contains(line, "auth=") {
			lines[i] = strings.Replace(line, "AUTH_NONE", info.FdeInfo.Mode(), 1)
		}
	}

	output := strings.Join(lines, "\n")
	err = ioutil.WriteFile(efiConfig, []byte(output), 0644)
	if err != nil {
		log.Print(err)
		return false
	}

	return true
}

func finishDiskInstallation(info InstallInfo, controlPath string) bool {
	if os.MkdirAll("/tmp/mnt", 0777) != nil {
		log.Print("Failed to create dom0 rootfs mount point.")
		return false
	}
	defer os.Remove("/tmp/mnt")

	if !mountPartition(controlPath, "/tmp/mnt") {
		log.Print("Failed to mount dom0 rootfs.")
		return false
	}
	defer unmountPartition("/tmp/mnt")

	if info.Upgrade && !modifyEfiConfiguration(info, "/tmp/mnt/boot/efi.cfg") {
		log.Print("Failed to modify EFI configuration.")
		return false
	}

	if !setupEsp(espPartition(info.BlockDevice),
		"/tmp/mnt/usr/lib64/efi/xen.efi",
		"/tmp/mnt/usr/lib64/efi/xen.efi",
		"/tmp/mnt/boot/efi.cfg",
		"/tmp/mnt/boot/bzImage",
		"/tmp/mnt/boot/initramfs.gz") {
		return false
	}

	if !setupEFIBootEntry(info.BlockDevice,
		"/EFI/BOOT/BOOTX64.EFI",
		"Redfield") {
		return false
	}

	return true
}

func closeDiskInstallation() {
	if !closeLuksVolume("storage") {
		log.Print("Failed to close encrypted storage partition.")
	}
}

func verifyLocalInstallInfo(info *InstallInfo) bool {
	return ((info.FdeInfo.Password() != "") || info.Upgrade) &&
		checkSelectedDisk(info.BlockDevice)
}

func verifyRemoteInstallInfo(info *InstallInfo) bool {
	return info.RemoteAuth.Certificate != "" &&
		info.RemoteAuth.Server != "" &&
		verifyLocalInstallInfo(info)
}

func init() {
	logWriter, e := syslog.New(syslog.LOG_NOTICE, "installer")

	if e == nil {
		log.SetOutput(logWriter)
	}

	productUUID, err := ioutil.ReadFile("/sys/class/dmi/id/product_uuid")
	if err != nil {
		log.Print("Failed to find a suitable backup password (no product-uuid).")
		os.Exit(-1)
	}

	ProductUUID = string(productUUID)
}

// ExistingInstallation check a disk for an existing installation
func ExistingInstallation(d string) bool {
	activatePhysicalVolume, err := LoggedCommand("pvscan").CombinedOutput()
	log.Print(string(activatePhysicalVolume))
	if err != nil {
		// Non-fatal
		log.Print("Failed to activate physical volume.")
	}

	displayPhysicalVolume, err := LoggedCommand("pvdisplay", physicalVolumePartition(d)).CombinedOutput()
	log.Print(string(displayPhysicalVolume))
	if err != nil {
		// Non-fatal
		log.Print("Failed to activate physical volume.")
	}

	detectVolumeGroup, err := LoggedCommand("vgdisplay", "dom0").CombinedOutput()
	log.Print(string(detectVolumeGroup))
	if err != nil {
		log.Print("Failed to detect volume group dom0")
		return false
	}

	return true
}

func downloadFile(url string, destination string) error {
	out, err := os.Create(destination)
	if err != nil {
		return err
	}
	defer out.Close()

	// #nosec
	resp, err := http.Get(url)
	if err != nil {
		return err
	}
	defer resp.Body.Close()

	_, err = io.Copy(out, resp.Body)
	if err != nil {
		return err
	}

	return nil
}

type installImage struct {
	Source      string `json:"source"`
	Destination string `json:"destination"`
	Hash        string `json:"hash,omitempty"`
}

type installImageList struct {
	Images []installImage `json:"images"`
}

func fetchImages(info InstallInfo) (installImageList, error) {
	var imageList installImageList

	u, err := url.Parse(info.RemoteAuth.Server)
	if err != nil {
		log.Print(err.Error())
		return imageList, err
	}

	u.Path = path.Join(u.Path, "manifest.json")
	err = downloadFile(u.String(), "/tmp/manifest.json")
	if err != nil {
		log.Print(err.Error())
		return imageList, err
	}

	manifest, err := ioutil.ReadFile("/tmp/manifest.json")
	if err != nil {
		log.Print(err.Error())
		return imageList, err
	}

	err = json.Unmarshal(manifest, &imageList)
	if err != nil {
		log.Print(err.Error())
		return imageList, err
	}

	for i := range imageList.Images {
		dir, _ := path.Split(imageList.Images[i].Destination)
		err = os.MkdirAll(dir, os.ModeDir)
		if err != nil {
			log.Print(err.Error())
			return imageList, err
		}

		err = downloadFile(imageList.Images[i].Source, imageList.Images[i].Destination+".new")
		if err != nil {
			log.Print(err.Error())
			return imageList, err
		}
	}

	return imageList, nil
}

func installImages(imagePath string) bool {
	images, err := ioutil.ReadDir(imagePath)
	if err != nil {
		return false
	}

	err = os.MkdirAll("/storage/images", os.ModeDir)
	if err != nil {
		log.Print(err.Error())
		return false
	}

	for _, image := range images {
		if image.Name() != "control.ext4" {
			srcPath := path.Join(imagePath, image.Name())
			dstPath := path.Join("/storage/images", image.Name())

			if !copyFile(srcPath, dstPath) {
				log.Print("Failed to copy image: " + image.Name())
				return false
			}
		} else {
			srcPath := path.Join(imagePath, image.Name())
			dstPath := path.Join("/storage", image.Name())

			if !copyFile(srcPath, dstPath) {
				log.Print("Failed to copy image: " + image.Name())
				return false
			}
		}
	}

	return true
}

// UpgradeControlDomain updates the existing dom0 rootfs only
func UpgradeControlDomain(controlDomainPath string) bool {
	if _, err := os.Stat("/storage/control.ext4"); err != nil {
		log.Print("Cannot perform upgrade, redfield is not running.")
		return false
	}

	if _, err := os.Stat(controlDomainPath); err != nil {
		log.Print("Cannot perform upgrade, no new control domain found.")
		return false
	}

	var info InstallInfo

	info.BlockDevice = blockDevicePathWithPhysicalVolume(getPhysicalVolume())

	if !finishDiskInstallation(info, controlDomainPath) {
		log.Print("Failed to finalize target disk for redfield installation.")
		return false
	}

	err := os.Rename(controlDomainPath, "/storage/control.ext4")
	if err != nil {
		log.Print("Failed to move image to final location.")
		return false
	}

	return true
}

// UpgradeLocalInstall updates an existing platform, preserving user VMs
func UpgradeLocalInstall(info InstallInfo) bool {
	if !verifyLocalInstallInfo(&info) {
		log.Print("Failed to verify installation information for upgrade.")
		return false
	}

	if !scanLogicalVolumes() {
		log.Print("Failed to scan logical volumes for upgrade")
		return false
	}

	if !activateLogicalVolumes("dom0") {
		log.Print("Failed to activate the dom0 volume group.")
		return false
	}

	if !info.FdeInfo.OpenDevice("/dev/mapper/dom0-storage", "storage") {
		log.Print("Failed to open storage volume.")
		return false
	}
	defer info.FdeInfo.CloseDevice("storage")

	if !mountStoragePartition() {
		return false
	}
	defer unmountStoragePartition()

	if !installImages(info.ImagePath) {
		log.Print("Failed to install images to storage partition.")
		return false
	}

	if !finishDiskInstallation(info, "/storage/control.ext4") {
		log.Print("Failed to finalize target disk for redfield installation.")
		return false
	}

	return true
}

// UpgradeRemoteInstall updates an existing platform, preserving user VMs
func UpgradeRemoteInstall(info InstallInfo) bool {
	if !verifyRemoteInstallInfo(&info) {
		log.Print(info.RemoteAuth.Certificate + " - " + info.RemoteAuth.Server)
		log.Print("Failed to verify remote installation information.")
		return false
	}
	log.Print("Performing remote install for device " + ProductUUID)

	if _, err := os.Stat("/storage/control.ext4"); err != nil {
		if !scanLogicalVolumes() {
			log.Print("Failed to scan logical volumes for upgrade")
			return false
		}

		if !activateLogicalVolumes("dom0") {
			log.Print("Failed to activate the dom0 volume group.")
			return false
		}

		if !info.FdeInfo.OpenDevice("/dev/mapper/dom0-storage", "storage") {
			log.Print("Failed to open storage volume.")
			return false
		}
		defer info.FdeInfo.CloseDevice("storage")

		if !mountStoragePartition() {
			return false
		}
		defer unmountStoragePartition()
	}

	imageList, err := fetchImages(info)
	if err != nil {
		log.Print("Failed to fetch images for installation.")
		return false
	}

	if !finishDiskInstallation(info, "/storage/control.ext4.new") {
		log.Print("Failed to finalize target disk for redfield installation.")
		return false
	}

	for i := range imageList.Images {
		err = os.Rename(imageList.Images[i].Destination+".new", imageList.Images[i].Destination)
		if err != nil {
			log.Print("Failed to move image to final location.")
			return false
		}
	}

	return true
}

// LocalInstall perform an installation using images from /images
func LocalInstall(info InstallInfo) bool {
	if !verifyLocalInstallInfo(&info) {
		log.Print("Failed to verify installation information.")
		return false
	}

	log.Print("Performing local install for device " + ProductUUID)
	if !prepareDiskInstallation(info) {
		log.Print("Failed to prepare target disk for redfield installation.")
		return false
	}
	defer closeDiskInstallation()

	if !mountStoragePartition() {
		return false
	}
	defer unmountStoragePartition()

	if !installImages(info.ImagePath) {
		log.Print("Failed to install images to storage partition.")
		return false
	}

	if !finishDiskInstallation(info, "/storage/control.ext4") {
		log.Print("Failed to finalize target disk for redfield installation.")
		return false
	}

	return true
}

// RemoteInstall perform an installation fetching images from a remote management server
func RemoteInstall(info InstallInfo) bool {
	if !verifyRemoteInstallInfo(&info) {
		log.Print("Failed to verify remote installation information.")
		return false
	}
	log.Print("Performing remote install for device " + ProductUUID)

	if !prepareDiskInstallation(info) {
		log.Print("Failed to prepare target disk for redfield installation.")
		return false
	}
	defer closeDiskInstallation()

	if !mountStoragePartition() {
		return false
	}
	defer unmountStoragePartition()

	imageList, err := fetchImages(info)
	if err != nil {
		log.Print("Failed to fetch images for installation.")
		return false
	}

	if !finishDiskInstallation(info, "/storage/control.ext4.new") {
		log.Print("Failed to finalize target disk for redfield installation.")
		return false
	}

	for i := range imageList.Images {
		err = os.Rename(imageList.Images[i].Destination+".new", imageList.Images[i].Destination)
		if err != nil {
			log.Print("Failed to move image to final location.")
			return false
		}
	}

	return true
}
