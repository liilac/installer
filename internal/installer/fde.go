// Copyright 2018 Assured Information Security, Inc. All Rights Reserved.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package installer

import (
	"log"
	"os/exec"
)

// FdeFunctions Interface for handling disk encryption, authentication modes
// can implement this to provide authentication options for full disk
// encryption
type FdeFunctions interface {
	EncryptDevice(device string) bool
	OpenDevice(device string, name string) bool
	CloseDevice(name string) bool
	Mode() string
	Password() string
}

// FdeAuthInfo Base authorization information for full disk encryption
type FdeAuthInfo struct {
	ModeName string
	Password string
}

// FdePasswordInfo Password authorization information for full disk encryption
type FdePasswordInfo struct {
	Auth FdeAuthInfo
}

// EncryptDevice Encrypt device using luks and a password
func (f FdePasswordInfo) EncryptDevice(device string) bool {
	luksFormat := exec.Command("cryptsetup", "-q", "luksFormat", device, "-d", "-")
	stdin, err := luksFormat.StdinPipe()
	if err != nil {
		log.Print("Failed to get stdin for storage partition encryption.")
		return false
	}

	go func() {
		defer stdin.Close()
		_, err = stdin.Write([]byte(f.Auth.Password))
		if err != nil {
			log.Print("Failed to provide the storage partition password for encryption.")
		}
	}()

	_, err = luksFormat.CombinedOutput()
	if err != nil {
		log.Print("Failed to run luksFormat with password.")
		return false
	}

	return true
}

// OpenDevice Open a luks encrypted device using a password
func (f FdePasswordInfo) OpenDevice(device string, name string) bool {
	luksOpen := exec.Command("cryptsetup", "luksOpen", device, name, "-d", "-")
	stdin, err := luksOpen.StdinPipe()
	if err != nil {
		log.Print("Failed to supply password for decrypting storage.")
		return false
	}

	go func() {
		defer stdin.Close()
		_, err = stdin.Write([]byte(f.Auth.Password))
		if err != nil {
			log.Print("Failed to provide the storage partition password for decryption.")
		}
	}()

	_, err = luksOpen.CombinedOutput()
	if err != nil {
		log.Print("Failed to open storage partition using password.")
		return false
	}

	return true
}

// CloseDevice Close a luks encrypted device
func (f FdePasswordInfo) CloseDevice(name string) bool {
	luksClose, err := LoggedCommand("cryptsetup", "luksClose", name).CombinedOutput()
	log.Print(string(luksClose))

	return err == nil
}

// Mode Returns the authentication mode for an instance of the FdeFunctions interface
func (f FdePasswordInfo) Mode() string {
	return f.Auth.ModeName
}

// Password Return the password associated with an instance of the FdeFunctions interface
func (f FdePasswordInfo) Password() string {
	return f.Auth.Password
}

// FdeTokenInfo Token authorization information for full disk encryption
type FdeTokenInfo struct {
	Auth FdeAuthInfo
}

// EncryptDevice Encrypt device using luks and a Yubikey challenge-response
func (f FdeTokenInfo) EncryptDevice(device string) bool {
	initYubi, err := LoggedCommand("ykpersonalize", "-y", "-v", "-2", "-ochal-resp", "-ochal-hmac", "-ohmac-lt64", "-oserial-api-visible").CombinedOutput()
	log.Print(string(initYubi))
	if err != nil {
		return false
	}

	yubiFormat, err := LoggedCommand("ykfde-format", "-p", f.Auth.Password, "-q", device).CombinedOutput()
	log.Print(string(yubiFormat))
	return err == nil
}

// OpenDevice Open a luks encrypted device using a Yubikey challenge-response
func (f FdeTokenInfo) OpenDevice(device string, name string) bool {
	yubiOpen, err := LoggedCommand("ykfde-open", "-c", f.Auth.Password, "-d", device, "-n", name).CombinedOutput()
	log.Print(string(yubiOpen))
	return err == nil
}

// CloseDevice Close a luks encrypted device
func (f FdeTokenInfo) CloseDevice(name string) bool {
	yubiClose, err := LoggedCommand("cryptsetup", "luksClose", name).CombinedOutput()
	log.Print(string(yubiClose))
	return err == nil
}

// Mode Return the authentication mode string associated with an instance of the FdeFunctions interface
func (f FdeTokenInfo) Mode() string {
	return f.Auth.ModeName
}

// Password Return the password string associated with an instance of the FdeFunctions interface
func (f FdeTokenInfo) Password() string {
	return f.Auth.Password
}
