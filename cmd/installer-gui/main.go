// Copyright 2018 Assured Information Security, Inc. All Rights Reserved.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package main

import (
	"bytes"
	"io/ioutil"
	"log"
	"log/syslog"
	"os"
	"os/exec"
	"strconv"
	"strings"

	"github.com/andlabs/ui"
	_ "github.com/andlabs/ui/winmanifest"
	"gitlab.com/redfield/installer/internal/installer"
)

var logWriter *syslog.Writer

// reboot issues 'reboot' command to restart the system.
func reboot() bool {
	rebootStderr, err := exec.Command("reboot").CombinedOutput()
	log.Print(string(rebootStderr))

	return err == nil
}

func validBlockDevices() []string {
	targetBlkdevs := make([]string, 1)
	rootMount, err := exec.Command("mount").CombinedOutput()
	log.Print(string(rootMount))
	var rootDevice string

	if err != nil {
		return targetBlkdevs
	}

	rootMountOutput := bytes.Split(rootMount, []byte("\n"))
	for _, mount := range rootMountOutput {
		if bytes.Contains(mount, []byte("on / type")) {
			rootDevice = string(mount)
		}
	}

	blkdevs, err := ioutil.ReadDir("/sys/block")

	if err != nil {
		return targetBlkdevs
	}

	for _, blkdev := range blkdevs {
		blkdevName := blkdev.Name()
		if strings.Contains(blkdevName, "dm") ||
			strings.Contains(blkdevName, "loop") ||
			strings.Contains(blkdevName, "ram") ||
			strings.Contains(blkdevName, "sr") ||
			strings.Contains(rootDevice, blkdevName) {
			log.Print("Ignoring device " + blkdevName)
		} else {
			targetBlkdevs = append(targetBlkdevs, blkdevName)
		}
	}

	return targetBlkdevs
}

type installerGuiState struct {
	mainwin               *ui.Window
	vbox                  *ui.Box
	installTypeCombobox   *ui.Combobox
	blockDeviceCombobox   *ui.Combobox
	managementCredentials *ui.Group
	entryForm             *ui.Form
	usernameEntry         *ui.Entry
	passwordEntry         *ui.Entry
	serverEntry           *ui.Entry
	fdePasswordEntry      *ui.Entry
	fdeAuthOptionCombobox *ui.Combobox
	hbox                  *ui.Box
	exitButton            *ui.Button
	installButton         *ui.Button
	upgradeButton         *ui.Button
	blklist               []string
	authOptions           []string
}

var globalState installerGuiState

func installButtonClicked(button *ui.Button) {
	ip := ui.NewProgressBar()
	ip.SetValue(-1)
	globalState.vbox.Append(ip, false)

	if globalState.usernameEntry == nil &&
		globalState.passwordEntry == nil &&
		globalState.serverEntry == nil {
		globalState.usernameEntry = ui.NewEntry()
		globalState.passwordEntry = ui.NewPasswordEntry()
		globalState.serverEntry = ui.NewEntry()
	}

	mode := globalState.authOptions[globalState.fdeAuthOptionCombobox.Selected()]

	var fdeInfo installer.FdeFunctions

	if mode == "Token" {
		fdeInfo = installer.FdeTokenInfo{
			Auth: installer.FdeAuthInfo{
				Password: globalState.fdePasswordEntry.Text(),
				ModeName: mode,
			},
		}
	} else {
		fdeInfo = installer.FdePasswordInfo{
			Auth: installer.FdeAuthInfo{
				Password: globalState.fdePasswordEntry.Text(),
				ModeName: mode,
			},
		}
	}

	info := installer.InstallInfo{
		BlockDevice: "/dev/" + globalState.blklist[globalState.blockDeviceCombobox.Selected()],
		DeviceUUID:  installer.ProductUUID,
		Upgrade:     false,
		RemoteAuth: installer.RemoteInstallAuth{
			Username:    globalState.usernameEntry.Text(),
			Password:    globalState.passwordEntry.Text(),
			Server:      globalState.serverEntry.Text(),
			Certificate: "/etc/ca-certificates/cert.pem"},
		FdeInfo:   fdeInfo,
		ImagePath: "/images",
	}

	if globalState.serverEntry.Text() == "" {
		if !installer.LocalInstall(info) {
			ui.MsgBoxError(globalState.mainwin, "Local installation failed.", "!")
		} else if !reboot() {
			ui.MsgBoxError(globalState.mainwin, "Reboot failed!", "Please reboot manually.")
		}
	} else {
		if !installer.RemoteInstall(info) {
			ui.MsgBoxError(globalState.mainwin, "Remote installation failed.", "!")
		} else if !reboot() {
			ui.MsgBoxError(globalState.mainwin, "Reboot failed!", "Please reboot manually.")
		}
	}
	ui.Quit()

}

func upgradeButtonClicked(button *ui.Button) {
	ip := ui.NewProgressBar()
	ip.SetValue(-1)
	globalState.vbox.Append(ip, false)

	if globalState.usernameEntry == nil &&
		globalState.passwordEntry == nil &&
		globalState.serverEntry == nil {
		globalState.usernameEntry = ui.NewEntry()
		globalState.passwordEntry = ui.NewPasswordEntry()
		globalState.serverEntry = ui.NewEntry()
	}

	mode := globalState.authOptions[globalState.fdeAuthOptionCombobox.Selected()]

	var fdeInfo installer.FdeFunctions

	if mode == "Token" {
		fdeInfo = installer.FdeTokenInfo{
			Auth: installer.FdeAuthInfo{
				Password: globalState.fdePasswordEntry.Text(),
				ModeName: mode,
			},
		}
	} else {
		var password string

		if globalState.fdePasswordEntry.Text() == "" {
			password = installer.ProductUUID
		} else {
			password = globalState.fdePasswordEntry.Text()
		}

		fdeInfo = installer.FdePasswordInfo{
			Auth: installer.FdeAuthInfo{
				Password: password,
				ModeName: mode,
			},
		}
	}

	info := installer.InstallInfo{
		BlockDevice: "/dev/" + globalState.blklist[globalState.blockDeviceCombobox.Selected()],
		DeviceUUID:  installer.ProductUUID,
		Upgrade:     true,
		RemoteAuth: installer.RemoteInstallAuth{
			Username:    globalState.usernameEntry.Text(),
			Password:    globalState.passwordEntry.Text(),
			Server:      globalState.serverEntry.Text(),
			Certificate: "/etc/ca-certificates/cert.pem"},
		FdeInfo:   fdeInfo,
		ImagePath: "/images",
	}

	if globalState.serverEntry.Text() == "" {
		if !installer.UpgradeLocalInstall(info) {
			ui.MsgBoxError(globalState.mainwin, "Local upgrade failed.", "!")
		}
	} else if !installer.UpgradeRemoteInstall(info) {
		ui.MsgBoxError(globalState.mainwin, "Remote upgrade failed.", "!")
	}

	ui.Quit()

}

func blockDeviceSelected(box *ui.Combobox) {
	if installer.ExistingInstallation("/dev/" + globalState.blklist[globalState.blockDeviceCombobox.Selected()]) {
		globalState.upgradeButton = ui.NewButton("Upgrade")
		globalState.hbox.Append(globalState.upgradeButton, false)
		globalState.upgradeButton.OnClicked(upgradeButtonClicked)
	} else if globalState.upgradeButton != nil {
		globalState.hbox.Delete(2)
		globalState.upgradeButton = nil
	}
}

func installTypeSelected(box *ui.Combobox) {
	if globalState.installTypeCombobox.Selected() == 1 {
		globalState.usernameEntry = ui.NewEntry()
		globalState.passwordEntry = ui.NewPasswordEntry()
		globalState.serverEntry = ui.NewEntry()

		globalState.entryForm.Append("Username: ", globalState.usernameEntry, false)
		globalState.entryForm.Append("Password: ", globalState.passwordEntry, false)
		globalState.entryForm.Append("Server: ", globalState.serverEntry, false)
	} else {
		if globalState.serverEntry != nil {
			globalState.entryForm.Delete(3)
			globalState.serverEntry = nil
		}

		if globalState.passwordEntry != nil {
			globalState.entryForm.Delete(2)
			globalState.passwordEntry = nil
		}

		if globalState.usernameEntry != nil {
			globalState.entryForm.Delete(1)
			globalState.usernameEntry = nil
		}
	}
}

func makeInstallerPage(state *installerGuiState) {
	state.vbox = ui.NewVerticalBox()
	state.vbox.SetPadded(true)

	state.vbox.Append(ui.NewLabel("Choose installation type:"), false)
	state.installTypeCombobox = ui.NewCombobox()

	state.installTypeCombobox.Append("Local")
	state.installTypeCombobox.Append("Managed")

	state.vbox.Append(state.installTypeCombobox, false)

	state.vbox.Append(ui.NewLabel("Choose installation target disk:"), false)
	state.blockDeviceCombobox = ui.NewCombobox()
	blkdevs := validBlockDevices()

	for _, blkdev := range blkdevs {
		if strings.Compare(blkdev, "") != 0 {
			model, _ := ioutil.ReadFile("/sys/block/" + blkdev + "/device/model")
			sizeStr, _ := ioutil.ReadFile("/sys/block/" + blkdev + "/size")
			sectors, _ := strconv.Atoi(string(sizeStr[:len(sizeStr)-1]))

			// Sectors to bytes
			size := sectors * 512

			// Bytes to gigabytes
			size /= (1024 * 1024 * 1024)

			globalState.blockDeviceCombobox.Append(string(model) + " (" + strconv.Itoa(size) + "GB [/dev/" + blkdev + "])")
			globalState.blklist = append(globalState.blklist, blkdev)
		}
	}

	state.vbox.Append(state.blockDeviceCombobox, false)
	state.vbox.Append(ui.NewHorizontalSeparator(), false)

	state.managementCredentials = ui.NewGroup("Management Credentials")
	state.managementCredentials.SetMargined(true)
	state.vbox.Append(state.managementCredentials, true)

	state.entryForm = ui.NewForm()
	state.entryForm.SetPadded(true)
	state.managementCredentials.SetChild(state.entryForm)

	state.fdeAuthOptionCombobox = ui.NewCombobox()
	state.authOptions = append(state.authOptions, "Password")
	state.authOptions = append(state.authOptions, "Token")
	state.fdeAuthOptionCombobox.Append("Password")
	state.fdeAuthOptionCombobox.Append("Token")
	state.entryForm.Append("Disk Encryption Type: ", globalState.fdeAuthOptionCombobox, false)

	state.fdePasswordEntry = ui.NewPasswordEntry()
	state.entryForm.Append("Disk Encryption Password: ", globalState.fdePasswordEntry, false)

	state.usernameEntry = nil
	state.passwordEntry = nil
	state.serverEntry = nil

	state.installTypeCombobox.OnSelected(installTypeSelected)
	state.hbox = ui.NewHorizontalBox()
	state.hbox.SetPadded(true)
	state.vbox.Append(state.hbox, false)

	state.exitButton = ui.NewButton("Exit")
	state.installButton = ui.NewButton("Install")

	state.blockDeviceCombobox.OnSelected(blockDeviceSelected)

	state.exitButton.OnClicked(func(*ui.Button) {
		ui.Quit()
	})

	state.installButton.OnClicked(installButtonClicked)

	state.hbox.Append(state.exitButton, false)
	state.hbox.Append(state.installButton, false)
}

func setupUI() {
	globalState.mainwin = ui.NewWindow("redfield Installer", 640, 480, true)
	globalState.mainwin.OnClosing(func(*ui.Window) bool {
		ui.Quit()
		return true
	})
	ui.OnShouldQuit(func() bool {
		globalState.mainwin.Destroy()
		return true
	})

	tab := ui.NewTab()
	globalState.mainwin.SetChild(tab)
	globalState.mainwin.SetMargined(true)

	makeInstallerPage(&globalState)

	tab.Append("Redfield Installer", globalState.vbox)
	tab.SetMargined(0, true)

	globalState.mainwin.Show()
}

func main() {
	installerArgs := os.Args[1:]
	var err error

	if len(installerArgs) == 0 {
		logWriter, err = syslog.New(syslog.LOG_INFO, "installer-gui")

		if err == nil {
			log.SetOutput(logWriter)
		}
	}

	log.Print("Starting Installer-Gui")

	err = ui.Main(setupUI)
	if err != nil {
		log.Print(err)
	}
}
