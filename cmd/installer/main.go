// Copyright 2018 Assured Information Security, Inc. All Rights Reserved.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package main

import (
	"flag"
	"fmt"
	"log"
	"log/syslog"

	"gitlab.com/redfield/installer/internal/installer"
)

var (
	upgrade        = flag.Bool("upgrade", false, "Upgrade if an existing installation exists.")
	localinstall   = flag.Bool("local", false, "Perform a local installation.")
	blockdevice    = flag.String("block-device", "", "Target block device to install redfield to.")
	upgradeMinimal = flag.String("upgrade-minimal", "", "Upgrade control domain (provide path).")
	username       = flag.String("username", "", "User name for remote installation authentication.")
	password       = flag.String("password", "", "Password for remote installation authentication.")
	server         = flag.String("server", "http://vps212462.vps.ovh.ca/redfield/images", "Server for remote installation authentication.")
	certificate    = flag.String("certificate", "/etc/ca-certificates/cert.pem", "Certificate for remote installation authentication.")
	fdepassword    = flag.String("fde-password", "", "Password for full disk encryption")
	twofactor      = flag.Bool("2fa", false, "Enable token based 2FA for full disk encryption.")
)

func main() {
	flag.Parse()
	logWriter, err := syslog.New(syslog.LOG_INFO, "redfield-installer")

	if err == nil {
		log.SetOutput(logWriter)
	}

	if *upgradeMinimal != "" {
		installer.UpgradeControlDomain(*upgradeMinimal)
		return
	}

	var fdeInfo installer.FdeFunctions

	if *twofactor {
		fdeInfo = installer.FdeTokenInfo{
			Auth: installer.FdeAuthInfo{
				Password: *fdepassword,
				ModeName: "Token",
			},
		}
	} else {
		if *password == "" {
			*password = installer.ProductUUID
		}

		fdeInfo = installer.FdePasswordInfo{
			Auth: installer.FdeAuthInfo{
				Password: *fdepassword,
				ModeName: "Password",
			},
		}
	}

	info := installer.InstallInfo{
		BlockDevice: *blockdevice,
		DeviceUUID:  installer.ProductUUID,
		Upgrade:     *upgrade,
		RemoteAuth: installer.RemoteInstallAuth{
			Username:    *username,
			Password:    *password,
			Server:      *server,
			Certificate: *certificate,
		},
		FdeInfo:   fdeInfo,
		ImagePath: "/images",
	}

	if *localinstall {
		if !*upgrade {
			fmt.Println("Initiating local install")
			installer.LocalInstall(info)
		} else {
			fmt.Println("Initiating local upgrade")
			installer.UpgradeLocalInstall(info)
		}
	} else {
		if !*localinstall && !*upgrade {
			fmt.Println("Initiating remote install")
			installer.RemoteInstall(info)
		} else {
			fmt.Println("Initiating remote upgrade")
			installer.UpgradeRemoteInstall(info)
		}
	}
}
